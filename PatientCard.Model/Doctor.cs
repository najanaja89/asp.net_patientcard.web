﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCard.Model
{
    public class Doctor
    {
        public int Id { get; set; }
        public string Speciality { get; set; }
        public string DocName { get; set; }
    }
}
