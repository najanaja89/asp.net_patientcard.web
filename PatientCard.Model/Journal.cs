﻿using System;

namespace PatientCard.Model
{
    public class Journal
    {
        public int Id { get; set; }
        public string Diagnosis { get; set; }
        public DateTime DateVisit { get; set; }
        public Patient Patient { get; set; }
        public Doctor Doctor { get; set; }
    }


}
