﻿namespace PatientCard.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class PatientCardContext : DbContext
    {
        // Контекст настроен для использования строки подключения "PatientCardContext" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "PatientCard.Model.PatientCardContext" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "PatientCardContext" 
        // в файле конфигурации приложения.
        public PatientCardContext()
            : base("name=PatientCardContext")
        {
            Database.SetInitializer(new DataInitializer());
        }

        public System.Data.Entity.DbSet<PatientCard.Model.Patient> Patients { get; set; }

        public System.Data.Entity.DbSet<PatientCard.Model.Journal> Journals { get; set; }

        public System.Data.Entity.DbSet<PatientCard.Model.Doctor> Doctors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            //modelBuilder
            //    .Entity<User>()
            //    .HasMany(user => user.Id)
            //    .WithRequired(order => order.User)

        }

        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}