﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PatientCard.Model;
using PatientCard.Web.ViewModel;

namespace PatientCard.Web.Controllers
{
    public class PatientsController : Controller
    {
        private PatientCardContext db = new PatientCardContext();

        // GET: Patients
        public ActionResult Index()
        {
            return View(db.Patients.ToList());
        }


        public ActionResult Signup(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = db.Patients.Find(id);
            JournalViewModel journalViewModel = new JournalViewModel
            {
                Patient = new PatientViewModel
                {
                    Id = patient.Id,
                    Name = patient.Name,
                    Iin = patient.Iin
                }
            };
            if (journalViewModel == null)
            {
                return HttpNotFound();
            }
            return View(journalViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signup(JournalViewModel journalViewModel)
        {
            if (ModelState.IsValid)
            {
                Doctor doctor = journalViewModel.Doctor;
                Patient patient = db.Patients.Find(journalViewModel.Patient.Id);
                //Patient patient = new Patient
                //{
                //    Name = journalViewModel.Patient.Name,
                //    Id = journalViewModel.Patient.Id,
                //    Iin = journalViewModel.Patient.Iin
                //};

                var journal = new Journal
                {
                    Diagnosis = journalViewModel.Diagnosis,
                    DateVisit = journalViewModel.DateVisit,
                    Patient = patient,
                    Doctor = doctor
                };

                db.Journals.Add(journal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }


        // GET: Patients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var doctor = db.Doctors.ToList();
            Patient patient = db.Patients.Find(id);
            //Doctor doctor = db.Doctors.Find
            JournalViewModel journalViewModel = new JournalViewModel
            {
                Patient = new PatientViewModel
                {
                    Id = patient.Id,
                    Name = patient.Name,
                    Iin = patient.Iin
                },
            };
            if (journalViewModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.Journals = db.Journals.Where(j => j.Patient.Id == id).ToList() as IEnumerable<Journal>;

            return View(journalViewModel);
        }

        // GET: Patients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Patients/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Iin")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                db.Patients.Add(patient);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(patient);
        }

        // GET: Patients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = db.Patients.Find(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        // POST: Patients/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Iin")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(patient).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(patient);
        }

        // GET: Patients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Patient patient = db.Patients.Find(id);
            if (patient == null)
            {
                return HttpNotFound();
            }
            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Patient patient = db.Patients.Find(id);
            db.Patients.Remove(patient);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
