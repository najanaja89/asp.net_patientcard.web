﻿using PatientCard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientCard.Web.ViewModel
{
    public class JournalViewModel
    {
        public PatientViewModel Patient { get; set; }
        public int PatientId { get; set; }
        public Doctor Doctor { get; set; }
        public string Diagnosis { get; set; }
        public DateTime DateVisit { get; set; }
    }
}